# Marian Golias „Wstępna nauka języka greckiego. Teksty”

## 1.
Λέγω. Ἀκούεις. Θύει. Οὐ θαυμάζομεν, ἀλλὰ λέγομεν. Οὐκ ἀγγέλλετε, ἀλλ’ ἀκούετε. Λέγουσι καὶ ἀκούσιν. Ἄγγελλε. Ἄγετε.

## 2. Rolnik (a)
Θρασύμαχος ὁ γεωργὸς ἔχει οἶκον καὶ ἀγρόν. Ὁ ἀγρὸς τοῦ γεωργοῦ μικρός ἐστιν. Ἀλέξανδρος τοῦ Θρασυμάχου ἐστὶν υἱός. Ὁ Θρασύμαχος τῷ υἱῷ λέγει· „Ὦ Ἀλέξανδρε, σύλλεγε χόρτον τῷ ἵππῷ.

## 3. Rolnik (b)
Ὁ Θρασύμαχος ἔχει υἱοὺς Φίλιππον καὶ Ἀλέξανδρον. Τοῖς υἱοῖς λέγει Θρασύμαχος· „Ὦ υἱοί, ἐν τῷ τῶν γεωργῶν βίῳ πολλοὶ πόνοι εἰσίν. Τῷ δὲ σπουδαίῳ ἀνθρώπῳ οἱ πόνοι οὔκ εἰσι φοβεροί.”

## 4. Rolnik (c)
Ἐν τῷ τοῦ Θρασυμάχου οἴκῳ πρόβατα καὶ ἵπποι εἰσίν. Τὸ πρόβατον ἄγει ὁ Φίλιππος. Ὁ Ἀλέξανδρος λύει τοὺς ἵππους ἀπὸ ζυγοῦ. Ὁ Θρασύμαχος παρέχει τοῖς υἱοῖς δῶρα. Τὰ τέκνα χαίρουσι τοῖς δώροις.

## 5. Gnomy
1. Κακὸν φέρουσι καρπὸν οἱ κακοὶ φίλοι.
2. Νόμιζ’ ἀδελφοὺς τοὺς ἀληθινοὺς φίλους.
3. Θησαυρός εἰσι τοῦ βίου πιστοὶ φίλοι.

## 6. Ateńczycy (a)
Οἱ Ἀθηναῖοι σπουδαῖοι ἦσαν. Ἀλλὰ καὶ νῦν οἱ τῶν Ἀθηναίων ἀγροὶ οὐ πολλοὺς καρποὺς φέρουσιν. Ὁ τῶν Ἀθηναίων στρατὸς ἀνδρεῖος ἦν. Τοῖς Ἀθηναίοις πολλοὶ πόλεμοι πρὸς τοὺς βαρβάρους ἦσαν, φοβεροὶ γὰρ ἦσαν πολέμιοι οἱ βάρβαροι.

## 7. Ateńczycy (b)
Οἱ ἀρχαῖοι Ἀθηναῖοι ἔκτιζον πολλὰ ἱερὰ τοῖς θεοῖς. Ἐν τοῖς βωμοῖς ἔθυον σῖτον καὶ ζῷα. Ἐν τῷ πολέμῷ ὁ τῶν βαρβάρων στρατὸς διέφθειρε τὰ τῶν Ἀθηναίων ἱερὰ καὶ τοὺς οἴκους. Ὦ Ἀθηναῖοι, μετὰ τὸν πόλεμον ἄλλα λαμπρὰ ἱερὰ καὶ οἴκους κατεσκευάζετε.

## 8. Sparta
Σπάρτη ἐστὶν ὲν τῇ Λακονικῇ. Ἡ τῶν στρατηγῶν γνώμη καὶ ἡ τοῦ στρατοῦ ἀρετὴ τὴν τῶν Λακεδαιμονίων ἀρχὴν ηὔξανον. Ἡ μὲν Σπάρτη ἦρχε τῆς Μεσσήνης, οἱ δὲ Μεσσήνιοι ἤχθαιρον τοὺς Λακεδαιμονίους καὶ τὴν Λακονικὴν πολλάκις διέφθειρον.  Ὁ τῶν Λακεδαιμονίων στρατὸς ἐν τῇ μάχῃ ἀνδρεῖος ἦν.

## 9. Teby
Θῆβαι ἐν τῆ Βοιωτίᾳ εἰσίν. Τὰς Θήβας ἔκτιζε Κάδομς. Λαμπραὶ δ’ ἦσαν αἱ τῶν Θηβῶν ἑπτὰ πύλαι καὶ ἡ ἄκρα Καδμεία. Ἡ τῶν στρατηγῶν καὶ τῆς στρατιᾶς ἀνδρεία ταῖς Θῆβαις ἐλευθερίαν ἔσῳζεν. Οἱ Ἀθηναῖοι τοῖς Θηβαίοις ἀμαθίαν ὠνείδιζον.

## 10. Ateny
Ἀθῆναί εἰσιν ἐν τῇ Ἀττικῇ. Οἱ Ἀθηναῖοι ἔχαιρον τῇ ἐπὶ τῇ ἀνδρείᾳ καὶ τῇ σοφίᾳ καὶ ταῖς τέχναις δόξῃ. Θαυμάζομεν τὴν τῆς δόξην ἐπιθυμίαν καὶ τὴν θεοσέβειαν τῶν Ἀθηναίων. Ἡ κατὰ  θάλατταν ἡγεμονία πολλοὺς χρόνους ἦν Ἀθηναίων. Πολλὰ γὰρ ἦν Ἀθηναίοις πλοῖα.

## 11. Homer
Πολλοὶ μὲν ἦσαν Ἑλληνικοὶ ποιηταί, πρῶτον δὲ ποιητὴν Ὅμηρον λέγουσιν. Ἦσαν δ’ Ὁμήρῳ, τῷ ποιητῇ, πολλαὶ μαθεταί. Τὸν Ὅμηρον ὀνομάζομεν τῶν Ἑλληνικῶν νεανιῶν διδάσκαλον, οἱ γὰρ νεανίαι ἐξεμάνθανον τοῦ ποιητοῦ τὸ τῶν περὶ Ἴλιον μαχῶν ἐγκώμιον καὶ τὴν Ὀδύσσειαν. Ὦ μαθητά, ἀναγίγνωσκε σπουδαίως τὰς τοῦ Ὁμήρου ῥαψῳδίας.

## 12. Muzy
Οἱ παλαιοὶ ἐνόμιζον ἐννέα Μοῦσας εἰναι. Οἱ ποιηταὶ λέγουσιν, ὅτι αἱ τῶν Μουσῶν οἰκίαι ἐν ταῖς τοῦ Παρνασσοῦ ἄκραις ἦσαν. Αἱ Μοῦσαι προὐστάτευον τῶν τεχνῶν καὶ τῶν ἐπιστημῶν. Τῶν Μουσῶν πρώτη μὲν ἦν Καλλιόπη, τῶν δ’ ἄλλων ἡ μὲν Θάλεια τῆς κωμῳδίας θεὰ ἦν, ἡ δὲ Μελπομένη τῆς τραγῳδίας, ἡ δὲ Πολύμνια τῆς χορεῖας, ἡ δ’ Οὐρανία τῆς ἀστρονομίας. Ταῖς δὲ Μούσαις οἱ παλαιοὶ ἔκτιζον Μουσεῖα.  Ἐν ταῖς Ἀθήναις ἦν ἱερὸν τῶν Μουσῶν.

## 13. Opis Aten
Aἱ μὲν τῶν πλουσίων Ἀθηνῶν ἀγυιαὶ μακραὶ καὶ καλαὶ ἦσαν, μακρὰ δὲ καὶ λαμπρὰ ἦν ἡ ἀγορὰ. Καλὰ ἔκτιζον οἱ Ἀθηναῖοι ἱερὰ, ἐν οἷς πολλὰ δῶρα ἥθροιζον τοῖς θεοῖς.  Φανερὸν δ’ ἦν τὸ λαμπρὸν τῆς θεᾶς ἱερὸν, ὅ ἐφ’ ὑψηλῆς ἄκρας ἦν. Ὅτε δ’ οἱ Ἀθεναῖοι τῇ θεᾷ ἑορτὰς ἦγον, ἐν τῇ ἀγορᾷ καὶ ἐν ταῖς ἀγυιαῖς ἦσαν πολλοὶ πολῖται καὶ ξένοι, ὅι τῇ θεᾷ θαυμαστὰ δῶρα ἔφερον.

## 14. Męstwo Spartan i Ateńczyków
Οἱ Ἀθηναῖοι καὶ οἱ Σπαριᾶται ἐπὶ τῇ ἀνδρεῖᾳ δικαίως θαυμάζονται ὑπὸ τῶν ποιητῶν. Ἐν γὰρ ταῖς μάχαις οὐ φεύγουσι τοὺς πολεμίους, ἀλλὰ μάχονται ἀνδρείως καὶ ἀμύνονται τοὺς πολεμίους. Ὁ τῶν Περσῶν δεσπότης δεινὴν στρατιὰν ἐπὶ τοὺς  Ἀθηναίους ἄγει, βούλεται γὰρ κατακαίνειν τὰς Ἀθήνας. Ὦ δέσποτα, περιγίγνῃ μὲν τῶν ἄλλων, ἀλλ’ οὐκ Ἀθηναίων καὶ Σπαρτιατῶν. Οἰόμεθα τὴν τῶν Ἀθηναίων καὶ τῶν Σπαρτιατῶν ἀνδρείαν θαυμαστὴν εἶναι.

## 15. Burza
Ἡ βροντὴ ἐξαίφνες ἀκούεται· αἱ δὲ νεφέλαι τὸν ἥλιον σκοτίζουσιν. Οἱ δὲ τῶν ἀγρῶν καὶ τῶν δένδρων καρποὶ τοῖς ἀνέμοις διαφθείρονται. Οἰ γεωργοὶ οἴομαι τοῦς θεοῦς τοῖς ἀνθρώποις ὀργιζεσθαι καὶ τὰ κακὰ πέμπειν. Εὐχόμενοι οὖν δῶρα τοῖς θεοῖς φέρουσιν. Ὦ Θρασύμαχε, σπέρχου. Σπέρχετε ἄλλοι γεωργοὶ· ἤδη γὰρ ὔει.

## 16. Dedal (a)
Ἀθάνατὸς ἐστι ἡ δόξα Δαιδάλου, τοῦ τῶν Ἀθηναίων τεχνίτου. Ὁ Δαίδαλος φονεύει τὸν τῆς ἀδελφῆς υἱὸν καὶ φευγει μετ’ Ἰκάρου, τοῦ υἱοῦ ἐκ τῆς ἠπείρου εἰς τὴν νῆσον Κρήτην. Διὰ δὲ τὴν ἐν τῇ τέχνῃ δόξαν θαυμάζεται ὑπὸ τοῦ τῆς νήσου δεσπότου καὶ φίλος γίγνεται αύτοῦ. Ἐν ῇ νήσῳ κατασκευάζει τὸν λαβύρινθον, οὗ ὁδοὶ ἦσαν πολλαὶ καὶ ἄβατοι. Πολλοὺς ἐνιαυτοὺς ἦν ὁ Δαίδαλος ἐν τῇ Κρήτῃ.

## 17. Dedal (b)
Μετὰ πολλοὺς χρόνους ὁ Δαίδαλος, πόθῳ τῶν Ἀθηνῶν φερόμενος, φεύγειν βούλεται ἐκ τῆς Κρήτης, ἀλλ’ ὁ τῆς νήσου δεσπότης κωλύει τὸν τεχνίτην. Ὁ οὖν Δαίδαλος πτερὰ συντάττει καὶ κηρῷ συνάπτει. Διὰ τῶν πτερῶν φεύγει ὑπὲρ τῆς θαλάττης μετὰ τοῦ Ἰκάρου. Ὁ δ’ Ἴκαρος, τοῖς τοῦ Δαιδάλου λόγοις οὐ πειθόμενος, προσπελάζει τῷ ἡλίῳ, ὤστε λύεται ὁ κηρός, καὶ εἰς θάλατταν καταπίπτει. Ὁ δὲ Δαίδαλος σῴζεται εἰς τὴν Σικελίαν.

## 18. Wyprawa Argonautów
Οἱ ποιηταὶ ἐνεκωμίαζον τοὺς Ἀργοναύτας νοῦ θ’ ἕνεκα ἀνδρείου καὶ μάλιστα διὰ τὸν πλοῦν, ὅς ἦν εἰς τὴν τῶν Κολχῶν γῆν. Χαλεπὸς γὰρ ἦν ὁ πλοῦς καὶ μεστὸς πολλῶν κινδύνων. Ἀλλ’ Ἀθηνᾶ καὶ Ἥρα καὶ Ἑρμῆς τοῖς νεανίαις πορευομένοις φίλοι ἦσαν. Οἱ δὲ νέοι ἀνδρείως ἔφερον τοὺς δεινοὺς τοῦ πλοῦ κινδύνους καὶ ἐπὶ Κόλχους μετὰ πολλὰς ἡεμέρας ἧκον.

## 19. Cztery wieki ludzkości
Τὴν πρώτην γενεὰν οἱ ποιηταὶ ὠνόμαζον χρυσῆν. Ὁ μὲν νοῦς τῶν ἀνθρώπων ἁπλοῦς ἦν, εὖνοι δ’ οἱ  θεοὶ τοῖς θνητοῖς ἦσαν, ὥστε ἡ γῆ τοῖς ἀνθρώποις αὐτόματος καρποὺς ἔφερεν. Τῇ δ’ ἀργυρᾷ καὶ χαλκῇ γενεᾷ τῶν θεῶν κακόνων γιγνομένων διὰ τὴν τῶν ἀνθρώπων ἀσέβειαν λῦπαι καὶ νόσοι ἐτάραττον τὸν βίον. Ἡ δὲ τελευταία γενεά, ὀνομαζομένη σιδηρᾶ, μεστὴ ἦν πολλῶν κακῶν.

## 20. Wychowanie spartańskie
Τῶν νομοθετῶν τῶν παλαιῶν Λυκοῦργος Σπαρτιάτης μάλιστα ἐθεραπεῦετο. Κατὰ τὸν νόμον αὐτοῦ τὰ τῶν Σπαρτιατῶν τέκνα εἰς γυμνάσια ἐπέμπετο καὶ ἐκεὶ ἐπαιδεύετο. Ἅπαξ τοῦ ἐνιαυτοῦ ἐν τῷ ἱερῷ τῆς θεᾶς οἱ νέοι ἐῤῥαπίζοντο καὶ ὅς ῥαπιζόμενος οὐκ ἔκλαιεν, πρῶτος ἐνομίζετο. Ὦ Σπάρτη, ἐπείθου τοῖς τοῦ Λυκούργου νόμοις, ἀθάνατον οὖν δόξαν τῆς ἀνδρείας ἔχεις.

## 21. Spartanie
Ἁπλοῦς ἦν ὁ τῶν Σπαρτιατῶν βίος. Ἐπείθοντο δ’ ἀεὶ τοῖς νόμοις οἱ Σπαρτιᾶται καὶ τὴν ἀνδρείαν πρώτην τῶν ἀρετῶν εἶναι ἐνόμιζον. Ἐπειδὴ αἰσχρὸν ᾤοντο τέχνας ἑτέρας μανθάνειν ἢ τὰς πρὸς τὸν πόλεμον χρησίμας, τοὺς μὲν ἀγροὺς οἱ δοῦλοι εἰργάζοντο τοῖς πολίταις, τὰς δὲ τέχνας οἱ περίοικοι ἐπετήδευον. Ὑπὸ τῶν πολιτῶν τὸ ἐν τῷ πολέμῳ θάνατος ὡς καλὸς ἐμακαρίζετο.

## 22. Arion (a)
Ὑπὸ Περιάνδρου, τοῦ τῆς Κορίνθου τυράννου, ἐξενίζετο Ἀρίων, ὁ κιθαρῳδός. Ὲπειδὴ δ’ ὁ κιθαρῳδὸς εἰς τὴν Ἰταλίαν ἦκεν ἡ μουσικὴ τέχνη τῷ Ἀρίωνι πολλῶν θεσαυρῶν πηγὴ ἦν. Πόθῳ δὲ φέρομενος τοῦ τῶν Κορινθίων ἡγεμόνος, ὃς τὸν Ἀρίονα ἔστεργεν, ἐβούλετο εἰς Κορίνθον ἔρχεσθαι. Ἐπειδὴ δὲ μάλιστα τοῖς Κορινθίοις ναύταις ἐπίστευεν, ἐπὶ πλοῖον αὐτῶν ἐπιβαίνει.

## 23. Arion (b)
Τοὺς δὲ ναύτας τῶν τοῦ Ἀρίωνος θεσαυρῶν ἐπιθυμία εἶχεν, ὥστε τὸν Ἀρίωνα εἰς τὴν θάλατταν ῥίπτουσιν. Ὁ δ’ Ἀρίων ηὔχετο θεῷ· „Ὦ δαῖμον, ὦ τῆς ἐμῆς τύχης ἡγεμών, πρόσταττε τοῖς τῆς ἁλὸς θερσίν, ἐπεὶ οἱ ἄνθρωποι κακόνοι εἰσίν, εἶναι ἐμοὶ σωτῆρας.” Ὁ δὲ θεὸς πέμπει δελφῖνας. Οἱ δὲ δελφῖνες τὸν Ἀρίωνα εἰς τὴν τῶν Κορινθίων χθόνα κομίζουσιν. Οἱ μὲν ναῦται, ἐπεὶ ἦκον εἰς τὴν Κόρινθον, ὑπὸ Περιάνδρου κολάζονται, εὐδαίμων δ’ ὁ Ἀρίων τοῖς θεοῖς τὰ σωτήρια θύει.

## 24. Delfy
Ἐν Δελφοῖς ἱερὸν ἦν Ἀπόλλωνος ἀρχαῖον καὶ μαντεῖον τοῦ θεοῦ. Τὸ δ’ ἱερὸν ἦν πλούσιον, ὑπὸ γὰρ τῶν μαντευομένων πολλὰ δῶρα προσεφέρετο Ἀπόλλωνι. Χρυσὸς καὶ λαμπροὶ κρατῆρες ἐφυλάττοντο ἐν πολλοῖς θησαυροῖς. Τοῖς μαντευομένοις ἀπεκρίνετο ἡ Πυθία, ἱέρεια. Ἐν Δελφοῖς ἐγίγνοντο ἀγῶνες μουσικῆς, ἐν οἷς οἱ ῥαψωδοὶ ἔλεγον τὰ τοῦ Ὁμήρου καὶ οἱ κιθαρῳδοὶ ἐκιθάριζον. Ἐγίγνετο δὲ καὶ γυμνικὸς ἀγών.

## 25. Przysłowia
Ἔστι παροίμια· γλαῦκα εἰς Ἀθήνας (φέρειν)· τοῦτ’ ἔστιν ἔργον μάταιον πράττειν. Ἐν ταῖς Ἀθήναις γὰρ πολλαὶ γλαῦκες καὶ πολλαὶ γλαυκῶν εἰκόνες ἦσαν. Οἰ Ἀθηναῖοι τὰς γλαῦκας τῆς Ἀθηνᾶς ἰερὰς εἶναι ἐνόμιζον. Καὶ ἡ Ἀθηναία δραχμὴ γλαῦξ ὠνομάζετο, ὡς γὰρ χαρακτὴρ ἦν ἐπὶ τῆς δραχμῆς τῇ μὲν κεφαλὴ τῆς Ἀθηνᾶς, τῇ δὲ γλαῦξ.

Ἄλλη παροιμία· Αἰθίοπα λευκαίνεις· τοῦτ’ ἔστιν ἔργον ἀνήνυτον πράττειν, ὁ γὰρ Αἰθίοψ οὐ λευκαἰνεται.

## 26. Hoplita
Ἐν τοῖς πρὸς τοὺς Πέρσας πολέμοις ἐθαυμάζετο ἡ τῶν Ἑλλήνων στρατιωτῶν, μάλιστα δὲ τῶν ὁπλιτῶν ἀνδεία. Ἦν δ’ ὁ ὁπλίτης τῶν Ἑλλήνων στρατιώτης· ἔφερε δὲ θώρακα καὶ κόρυν καὶ κνημῖδας καὶ ἀσπίδα καὶ λόγχον καὶ μάχαιραν. Τῷ μὲν θώρακι ἐσῴζετο τὸ στέργον, ταῖς κνεμῖσιν αἰ κνῆμαι. Οἱ μὲν ὁπλῖται πεζῇ ἐμάχοντο, συνεστρατεύοντο δὲ τοῖς ὁπλίταις καὶ γυμνῆτες.

## 27. Mowa wodza
Στρατηγὸς πρὸς τοὺς στρατιὠτας ἔλεξεν· „Ἐπεὶ ἤκουσα, ὦ στρατιῶται, ὅτι οἱ πολέμιοι στρατιὰν συνέλεξαν, τὴν ἡμετέραν πατρίδα διαφθείρειν βουλόμενοι, ἐκέλευσα ὑμᾶς ἔρχεσθαι. Ἀλλ’ οὖ φοβερὸς ἡμῖν ὁ πόλεμος, πολλοὺς γὰρ ἤδη πολεμίους ἑνικήσαμεν. Ἡμέτεροι δὲ σύμμαχοι, οἳ πολλάκις ἡμῖν ἐβοήθησαν, καὶ νῦν ἔτοιμοι εἰσι συστρατεύεσθαι. Ἡ νίκη παρ’ ἡμῖν.”

## 28. Mowa wodza po bitwie
Ἄρξομαι άπὸ σοῦ, ὦ πατρίς. Καλὴν δόξαν έκτήσω. Οἱ ἡμέτεροι στρατιῶται διεπράξαντο λαμππρὸν ἔργον. Ἐνίκησαν γὰρ καὶ ἐτιμωρήσαντο τὸν πολέμιον, ὃς μαχόμενος οὐδὲν ἐκτήσατο. Τοὺς δ’ ἡμετέρους στρατώτας οἱ ποιηταὶ ἀεὶ ἐπὶ τῇ ἀνδρείᾳ έγκομιάσονται καὶ ὑμνήσουσιν. Νῦν δὲ τὰ σωτήρια παρασκευάσομεν.

## 29. Lew
Τῶν ζῷων μάλιστα θαυμάζομεν τὸν λέοντα· τῷ γὰρ λέοντι βασιλικὸν σχῆμά ἐστιν. Τοῖς λέουσι δεινὴ τοῦ σώματος ῥώμη πρόσεστιν, ἥ τοῖς ἄλλοις θηρίοις καὶ τοῖς ἀνθρώποις ἐπικίνδυνος γίγνεται. Ἄξιος οὖν ἐστιν ὁ λέων τῶν ἀλλῶν ζῷων βασιλεύειν. Οἱ λέοντες τῆς μὲν νυκτὸς μάλιστα ἐκ τῆς ἐνέδρας ἐξέρχονται, τὰς δ’ ἡμέρας ἐν τοῖς σπηλαίοις μένουσιν.

## 30. Kodrus
Τοῖς Πελοποννησίοις, ἐπεὶ εἰς τὴν Ἀττικὴν ἐβουλεύσαντο στρατεύεσθαι, ὁ θεὸς λέγει μαντευομένοις, ὅτι νικήσουσιν, εἰ Κόδρον, ὅς τῆς Ἀττικῆς ἐβασίλευεν, σώσουσιν. Ὁ δὲ Κόδρος, ἐπεὶ τοῦτο ἔκουσεν, οὐ μέλλει, ἀλλ’ ἐνδύεται φαύλην ἐσθῆτα καὶ εἰς τὸ τῶν πολεμίων στρατόπεδον ἔρχεται. Ἐκεῖ ἐλοιδορήσατο τοῖς στρατιώταις, οἱ δ’ αὐτὸν κατέσφαξαν. Ἐπεὶ δ’ οἱ Πελοποννήσιοι ἥκουσαν, ὅτι ὁ Κόδρος οὕτως ἐτελεύτησεν, ἐν ὀλίγῳ ἀπεχώρησαν ἐκ τῆς Ἀττικῆς.

## 31. Lew i lis
Λέοντι γεραιῷ χαλεπὸν ἦν τροφὴν πορίσασθαι. Ἐβουλεύσατο οὖν ὧδε πρᾶξαι· μένων έν τῷ σπελαίῳ. λέγει, ὅτι τὸ σῶμα κακῶς ἔχει. Ἤλπιζε γὰρ φονεύσειν τὰ ζῷα ἐπισκεπτόμενα αὐτὸν. Καὶ  οὕτως πολλὰ ζῷα ἐφόνευσεν. Ἧκε δὲ καὶ ἡ ἀλώπηξ, ἀλλ’ ἔξω μένουσα τοῦ σπελαίου, ἠρῶτησε τὸν λέοντα, ὅπως ἔχει. Ὁ δὲ λέων, ἀκούσας τῆς ἀλώπεκος, ἔλεξεν· „Διὰ τί μέλλεις οὐδὲ βούλει ἐπισκέψασθαι ἐν ὀλίγῳ τελευτήσοντα;” Ἡ δ’ ἀλώπεξ· „Βλέπω γὰρ τὰ τῶν εἰσερχομένων ἴχνια, ἀλλ’ οὐ βλέπω τὰ τῶν ἐξερχομένων.”

## 32. Znaczenie wychowania fizycznego
Παντὸς νεανίου ἐστὶ τὸ σῶμα γυμνάζεσθαι. Πᾶς γὰρ ἄνθροπος, τὸ σῶμα γυμναζόμενος, ὑγιαίνει. Καὶ πολλοὶ μὲν τὰ σώματα εὖ ἔχοντες διὰ τοῦτο ἐκ τῶν πολεμικῶν ἀγώνων σῴζονται καὶ τὰ δεινὰ πάντα τοῦ πολέμου διαφεύγουσιν· πολλοὶ δ’ ἐν πάσῃ ἀπορίᾳ τῇ πατρίδι βοηθῆσαι δυνατοί εἰσιν. Καὶ ἔχουσι τῆς ἀνδρείας δόξαν, ἧς πάντων τῶν ἀγαθῶν μάλιστα ὀρεγόμεθα. Πᾶν δὲ τοὐναντιον συμβαίνει τοῖς κακῶς ἔχουσι τὰ σώματα.

## 33. U bankiera greckiego
Σώφρων. Χαῖρε, ὦ Πασίον.
Πασίων. Χαῖρε καὶ σύ· ἀλλὰ τίς εἶ καὶ τί δεῖ σοι;
Σ. Σώφρων μὲν ὀνομάζομαι· πορεύομαι δ’ εἰς Ἀθήνας, ἐπιβάτης ὢν τοῦ πλοίου τοῦ Ἡγιστράτου. Καὶ δεῖ με δανείζεσθαι τἀργυριον. Τίσιν ἀνθρώποις δανείζεις;
Π. Δανείζω τοῖς ἐγγυητὰς ἔχουσιν. Τίνας ἔχεις ἐνθάδε φίλους;
Σ. Ἀλλὰ τίνες ἀνθρὠπων φίλοι εἰσὶ ξένῳ γ’ ὄντι ἐν ξένῃ γῇ; Ἀλλ’ ἐκεῖ Ἡγἐστρατος ἔρχεται. Δεῦρο μοι, ὦ Ἡγέστρατε.
Ἡγέστρατος. Τί βούλει;
Π. Ἔχει χρήματα ὁ Σώφρων;
Ἡ. Καὶ πολλὰ γε· ἔστι γὰρ σῖτος αὐτοῷ ἐν ἐμῷ πλοίῳ.
Π. Εὖ γε· πάνυ μὲν οὖν δανείζω.
Σ. Γράφε τὰς συγγραφάς.
Π. Γράφω.

## 34. Fryksus i Hella
Ἀθάμας, ὁ τῆς Βοιωτίας βασιλεύων, πατὴρ ἦν Φρίξου καὶ Ἕλλης, μήτηρ δὲ τῶν παῖδον ἦν Νεφέλη. Μετὰ τὸν τῆς μητρὸς θάνατον ἡ δευτέρα γυνή, τοῖς τῆς Νεφέλης τέκνοις ἐπιβουλεύουσα, τῷ ἀνδὶ συνεβούλευε τὸν υἱὸν καὶ τὴν θυγατέρα φονεῦσαι. Ὁ δ’ Ἀθάμας τῇ γυναικὶ ἐπείθετο, ἀλλ’ Ἑρμῆς, τὰ τέκνα σῴζειν βουλόμενος, ἐπὶ χρυσομάλλου κριοῦ φερόμενα ὑπερεβίβασεν. Πορευομένων δὲ τῶν παίδων ἡ Ἕλλη καταπίπτει εἰς τὴν θάλατταν, ἣ ἀπ’ αὐτῆς Ἑλλήσποντος ὀνομάζεται.

## 35. Danaidy
Δαναῷ μέν, τῷ τῆς Λιβύης δεσπότῃ, πεντήκοντα θυγατέρα ἦσαν, Αἰγύπτῳ δέ, τῷ τοῦ Δαναοῦ ἀδελφῷ, πεντήκοντα υἱοί. Στασιαζόντων δ’ αὐτῶν περὶ τῆς ἀρχῆς ὁ Δαναὸς μετὰ τῶν θυγατέρων φεύγει εἰς τὴν Ἑλλάδα. Ἐκεῖ καὶ οἱ τοῦ Αἰγύπτου παῖδες ἧκον, τὰς τοῦ Δαναοῦ θυγατέρας νυμφεύεσθαι βουλόμενοι. Ὁ δὲ Δαναὸς, συνεχώρησε τὰς τοῦ θυγατέρας γυναῖκας αὐτῶν εἶναι, ἀλλὰ τῆς ἔχθρας οὐ παυσάμενος, ἐπέταξε ταῖς θυγατράσι τοὺς ἄνδρας φονεῦσαι. Αἱ οὖν γυναῖκες, πειθόμεναι τῷ πατρὶ, ἐφόνευσαν αὐτοὺς νυκτός. Τοῦ δὲ τῶν ἀνδῶν φόνου δίκην ἐν Ἅιδου τίνουσιν, ὕδωρ ἀεὶ ἐν κοσκίνῳ φέρουσαι.

## 36. Drakon
Δράκων, ὁ τῶν Ἀθηναίων νομοθέτες, πᾶσι τοῖς ἁμαρτάνουσι ζημίαν θάνατον ἔταξεν, ὥστε τοῦς λάχανα χλέψαντας ὁμοίος κολάζεσθαι τοῖς ἀνδροφόνοις. Δικαίως οὖν οἱ παλαιοὶ ἔλεγον, ὅτι αἵματι τοὺς νόμους ὁ Δράκων ἔγραψεν. Αὐτὸς ὁ Δράκων πυνθανομένοις, διὰ τί πᾶσι τοῖς ἀδικήμασι ζημίαν ἔταξε θάνατον, ἔλεξεν, ὅτι τὰ μὲν μικρὰ θανάτου ἄξια νομίζει, τοῖς δὲ μεγάλοις ἀδικήμασιν οὐκ ἔχει ἄλλην ζημίαν.

## 37. Partenon
Ὁ Παρθενών, τὸ τῆς Ἀθηνᾶς παρθένου ἱερόν, ὑπερεῖχε πάντων τῶν ἐν ταῖς Ἀθήναις ἱερῶν μεγέθει τε καὶ κάλλει. Πάντα οὖν τὰ τῶν Ἑλλήνων γένη ἐθαύμαζε τὸ ἱερὸν τοῦ τε κάλλους ἕνεκα καὶ τοῦ μεγέθους. Ἔνδον δ’ ἦν τὸ τῆς Ἀθηνᾶς ἄγαμα ἐκ χρυσοῦ καὶ ἐλέφαντος, ἔργον Φειδίου. Ἐν τοῖς Παναθηναίοις, ἀνὰ τέτταρα ἔτη γιγνομένοις, πολλὰ δῶρα ὑπὸ τῶν τῆς Ἑλλάδος ἐθνῶν τῇ θεᾷ ἐφέρετο.

## 38. Sofokles i Arystofanes
Πλησίον τοῦ Παρθενῶνος τοῦ ἐν Άθήναις τὸ θέατρον τὸ τοῦ Διονύσου ἦν. Ἐκεῖ ἐδίδασκεν ὁ μὲν Σοφοκλῆς τραγῳδίας, ὁ δ’ Ἀριστοφάνης κωμῳδίας. Πάντες οἱ Ἕλληνες ἐθαύμαζον τὸν Σοφοκλέα καὶ τὸν Ἀριστοφάνη, τοὺς ἐνδόξους ποιητάς. Μεγάλα οὖν καὶ κλυτὰ λέγεται τὰ τοῦ Σοφοκλέους καὶ τοῦ Ἀριστοφάνους ὀνόματα. Τῷ Σοφοκλεῖ χρόνοις ὕστερον οἱ Ἀθηναῖοι ἔθυον ὡς θεῷ καὶ εἰκόνας αὐτῷ πολλὰς ἱδύοντο. Τῶν δὲ τοῦ Σοφοκλέους τραγῳδιῶν ὑπερέχει μάλιστα Οἰδίπους Τύραννος.

## 39. Sfinks
Ἀτυχὲς ἦν τὸ τῶν Θηβαίων ἔθνος. Ἥρα γάρ, ἐγκρατὴς θεά, τιμωρίαν λαμβάνουσα παρὰ τῶν ἀτυχῶν ἀνθρώπωνμ ἐπέπεμψεν αὐτοῖς Σφίγγα, κεφαλὴν μὲν ἔχουσαν παρθένου, σῶμα δὲ λέοντος, πτερὰ δ’ ὄρνιθος. Ἡ Σφίνξ, ἐπὶ ὄρει πρανεῖ πρὸ τῶν πυλῶν καθεζομένη, προὔτεινεν μὲν αἴνιγμα τοῖς ἀτυχέσι πολίταις, τοὺς δὲ τὸ αἴνιγμα μὴ λύοντας ἀπὸ τοῦ ὄρους κατέῤῥιπτεν. Οὕτως δὲ πολλοὺς Θηβαίους ἐφόνευσεν. Τέλος δ’ Οἰδίπους ἧκε καί, λύσας τὸ αἴνιγμα, τοὺς ἀτυχεῖς Θηβαίους ἔσωσεν.

## 40. Achilles i Odyseusz
Παρὰ τοῖς Ἕλλησιν ἥρωες ἐλέγοντο οἱ ἡμίθεοι καὶ οἱ ἐπιφανεῖς ἄνθρωποι. Ἐνδόξους δ’ ἥρωας εἶναι Ἀχιλλέα καὶ Ὀδυσσέα, τοὺς τῶν Ἑλλήνων βασιλέας, λέγουσιν. Τῷ μὲν Ὀδυσσεῖ, τῷ τῆς Ἰθάκης ἥρωι, γονεῖς ἦσαν Λαέρτης καὶ Ἀντίκλεια, τῷ δ’ Ἀχιλλεῖ Πηλεὺς καὶ Θέτις. Ὁ μὲν Ἀχιλλεὺς ἐν μονομαχίᾳ ἐφόνευσεν Ἕκτορα Τρῶα, τὸν τοῦ βασιλέως υἱὸν, καὶ ἄλλους πολλοὺς τῶν Τρώων, ὁ δ’ Ὀδυσσεὺς ἀεὶ τοῖς Ἕλλησιν ὠφέλιμα συνεβούλευσεν.

## 41. Wieśniak i żmija
Γεωργοῦ υἱὸς ὑπ’ ὄφεως δάκνεται.  Τοῦ δὲ παιδὸς τελευτήσαντος ὁ πατὴρ λαμβάνει ἀξίνην καὶ τὸν ὄφιν κατασφάττειν βούλεται, ἀλλὰ τοῦ ὄφεως ἁμαρτάνει. Λαμβάνει δ’ οὖν ἄρτον καὶ ἄλας καὶ καταβάλλει πρὸ τῆς τῶν ὄφεων καταδύσεως. Εἶτα δὲ λέγει· „Ἔξερπε, ὦ ὄφι, καὶ πίστεις ἀποδέχου τῆς ἐμῆς συγγνώμης.” Ὁ δὲ ὄφις ἀποκρίνεται· „Οὐκ εἰσὶ πίστεις ἀνθρώπῳ, ὅς τέκνου τάφον, οὐδὲ ὄφει, ὅς ἀξίνην βλέπει.”

## 42. Bogowie greccy
Κρόνος υἱεῖς εἶχε Δία καὶ Ποσειδῶνα καὶ Πλούτωνα. Ὁ μὲν Ζεὺς παντὸς τοῦ κόσμου ἐβασίλευεν, Διὶ γὰρ ἐπείθοντο πάντες οἱ θεοὶ καὶ πάντες οἱ ἄνθρωποι· Ποσειδῶν δὲ τῆς θαλάττας ἦρχεν, Πλούτων δὲ τοῦ Ἅιδου δεσπότης ἦν. Ἁπόλλωνι τῷ τοῦ Διὸς καὶ Λητοῦς υἱεῖ, τῶν τεχνῶν καὶ τῆς ποιήσεως ἦν ἐπιμέλεια. Ἑρμῆς δὲ τῶν θεῶν ἄγγελος ἦν καὶ τῶν ἐμπόρον καὶ τῶν ῥητόρων καὶ τῶν γυμναστῶν ἐκήδετο. Ἄρης δε τοῦ πολέμου καὶ τῶν μαχῶν εἶχεν ἐπιμέλειαν.

## 43. Zeus i pszczoły
Μέλιτται ἔπεμψαν δώρῳ τῷ Διὶ πλῆθος γλυκέος μέλιτος καὶ ἔλεξεν· „Ὦ Ζεῦ, οἱ θρασεῖς ἄνθρωποι ἁρπάζουσι τὸ ἡμέτερον μέλι. Ὅτι δ’ ἡ τοῦ χειμῶνος τραχεῖα ὥρα ἥκει, τροφὴν οὐκ ἔχομεν. Πάρεχε οὖν ταῖς μελίτταις ὀξέα κέντρα. Ὀξέσι γὰρ κέντροις άποδιώξομεν τοὺς θρασεῖς ἀνθρώπους.” Ὁ δὲ θεός, ἡδόμενος μὲν δώρῳ, στέργων δὲ καὶ τοὺς ἀωθρώπους καὶ τὰς μελίττας, ἔλεξεν· „Ὦ μέλιτται, ἐπειδὴ βούλεσθε ὀξὺ κέντρον ἔχειν, ἔχετε τὸ κέντρον· ἀλλὰ λύπην τῷ ἀνθρώπῳ φεροῦσας τὰς μελίττας χρὴ τελευτῆσαι.”

## 44. Mysz wiejska i mysz miejska
Μῦς ἀρουραῖος ἀστικῷ γίγνεται φίλος μυὶ καὶ πρῶτος εἰς τὸν ἀγρὸν τὸν ἐξ ἄστεως μῦν κομίζει καὶ αὐτῷ φέρει τοὺς τῶν ἀγρῶν καρπούς. Ἀμειβόμενος δὲ τὴν ξενίαν ὁ ἀστικὸς εἰς ἄστυ τὸν ἀρουραῖον ἐκόμισεν. Ἥκουσι δ’ οἱ μύες πρὸς οἶκον ἀνδρὸς πλουσίου. Ὡς δὲ τοῦ τυροῦ ἅπτεσθαι ἐβούλοντο, ὁ ἄνθρωπος εἰς φυγὴν ἔτρεφε τοὺς μῦς. Καὶ τοσαυτάκις ἀπηλαύοντο, ὁσάκις ἐβούλοντο ἅπτεσθαι τοῦ τυροῦ. Καὶ τέλος ὁ ἀρουραῖος· „Ἀπερχομαι, μᾶλλον γὰρ βούλομαι τὸν μετ’ ἀσφαλείας παρεχόμενον ἄρτον ἤ τὸν ἐν ἄστει ἐπικίνδυνον τυρόν.”

## 45. W porcie
Ἐν τῷ λιμένι πολλαὶ νῆές εἰσιν. Ἡ μὲν ναῦς μακρά ἐστιν, αἱ δ’ ἄλλαι στρογγύλαι. Κατὰ τὰς ναῦς θόρυβος ἀκούεται, οἱ γὰρ κυβερνῆται προστάγματα τοῖς ναύταις προστάττουσιν, οἱ δὲ ναῦται μεταχειρίζουσι τὰ τῆς νεὼς ὅπλα. Ἐν τῇ μακρᾷ νηὶ οἱ στρατιῶται τὴν ἄγκυραν αἴρουσιν, ἐν δὲ ταῖς στρογγύλαις ναυσὶν οἱ ἔμποροι συσκευάζουσιν οἶνον καὶ σῖτον καὶ δέρματα καὶ ἄλλα τὰ πρὸς ἔμπορίαν.

## 46. Gnomy
1. Ὦ παῖ, βραχεῖα τέρψις ἡδονῆς κακῆς.
2. Βούλου γονεῖς πρὸ παντὸς ἐν τιμαῖς ἔχειν.
3. Γύμναζε παῖδας, ἄνδρας οὐ γὰρ γυμνάσεις.

## 47. Solon
Σόλων Ἀθηναῖος διὰ τὴν σοφίαν ἔνδοξος ἦν ἐν τοῖς Ἀθηναῖοις. Δύναμιν δ’ ἐν τῇ πόλει κτησάμενος τὸν δῆμον ἠλευθέρωσεν ἐν βραχεῖ χρόνῳ· ἐκώλυσε γὰρ δανείζειν ἐπὶ τοῖς σώμασι καὶ χρεῶν ἀποκοπὰς ἐποίησεν, ἃ σεισάχθειαν οἱ Ἀθηναῖοι ὠνόμαζον, ὡς ἀποσεισάμενοι τὸ ἄχθος. Τὰς δ’ ἀρχὰς ἐποίησε κληρωτὰς ἐκ προκρίτων. Διατάξας δὲ τὰ τῆς πόλεως, ἐκ τῆς πατρίδος ἀπεχώρησεν.

## 48. Krezus
Κροῖσος πολὺν χρόνον ἐβασίλευεν τῶν Λυδῶν. Ἦν δὲ θαυμαστὸς καὶ ἔνδοξος ἐπὶ τῷ πολλῷ πλοῦτῳ καὶ τῇ τῆς χώρας μεγάλης οὔσης εὐδαιμονίᾳ. Πολλοὶ τοίνυν καὶ μεγάλοι βασιλεῖς ἐκολάκευον τὸν Κροῖσον. Ἐπειδὴ Κῦρος, ὁ μέγας τῶν Περσῶν βασιλεύς, Ἀστυάγη, τὸν τῶν Μήδων βασιλέα, ἔπαυσε τῆς ἀρχῆς, ὁ Κροῖσος, ἐβουλεύετο τὴν τοῦ Κύρου μεγάλην βασιλείαν καταλύειν. Συλλέξας οὖν μέγα στράτευμα, ἐπὶ τοὺς Πέρσας ἐστρατεύσατο. Ὁ δὲ Κῦρος, ἐν τῇ μάχη νικήσας αὐτόν, ἐζώγρησεν.

## 49. Solon i Krezus (a)
Κροίσου ἔτι τῶν Λυδῶν βασιλεύοντος ἧκεν εἰς Σάρδεις, τὴν πλουσιωτάτην τῆς Λυδίας πόλιν, Σόλων σωφρονέστατος καὶ ἐνδοξότατος τῶν τότε Ἀθηναίων ὤν. Οἱ δὲ θεράποντες τοῦ βασιλέως κελεύσαντος τόν Σόλωνα περιῆγον κατὰ τούς θεσαυρούς, λαμπροτέρους ὄντας τῶν τοῦ Περσῶν βασιλέως. Θεασάμενον δ’ ἀυτὸν πάντα ἐρώτησεν ὁ Κροίσος· „Ὦ ξένε Ἀθηναῖε, ἤκουσα, ὄτι πάντων τῶν Ἑλλήνων σοφώτατος εἶναι νομίζῃ. Νῦν οὖν βούλομαι ἀκοῦσαι, τίνα οἴει ἐμοῦ γ’ εὐτυχέστερον εἶναι.” Ὁ δὲ Σόλων λέγει· „Ὦ βασιλεῦ, Τέλλον Ἀθηναῖον σοῦ ὀλβιώτερον εἶναι νομίζω.”

## 50. Solon i Krezus (b)
Ταῦτα ἀκούσας ὁ Κροῖσος ἐπηρώτησεν· „Τὶ δὲ τῷ Τέλλῳ τύχην καλλίονα καὶ ἡδίονα εἶναι νομίζεις;” Ὁ δὲ Σόλων· „Τῷ Τέλλῶ παῖδες ἦσαν κάλλιστοι καὶ βέλτισοι. Τελευτὴ δὲ τοῦ βίου λαμπροτάτη ἦν καὶ ἡδίστη· ῥᾷον δή ἐστι μέγιστον πλοῦτον κτῆσασθαι ἢ καλῶς τελευτῆσαι. Ὁ δὲ Τέλλος τοὺς πολεμίους νικήσας, ἐτελεύτησεν.” „Τίνα δέ”, ἠρώτησεν ὁ Κροῖσος, „δεύτερον μετ’ ἐκεῖνον νομίζεις εὐδαιμονέστατον.” Ὁ δ’ ἔλεξεν· „Τὰ δὲ δευτερεῖα φέρουσι Κλέοβίς τε καὶ Βίτων, οἳ ἐν ἱερῷ ῥᾷστον θάνατον ἐτελεὐτησαν, τὴν μητέρα τιμήσαντες. Τί γάρ ἐστι μεῖζον δώρον τῶν θεῶν ἢ καλὸς θάνατος;”

## 51. Solon i Krezus (c)
Ὁ δὲ Κροῖσος ἤδη χαλεπαίνων· „Ὦ ξένε Ἀθηναῖε”, ἔφη, "τί τὸν καλὸν θάνατον ὀνομάζεις μέγιστον δῶρον;” Ὁ δὲ ἔλεξεν· „Ὦ βασιλεῦ, οὔτε σε, εἰ καὶ πλοῦτον πλείω ἔχεις καὶ δύναμιν μείζω τῶν ἄλλοων βασιλέων, οὔτε ἄλλον ἄνθρωπον πρὸ τῆς τοῦ βίου τελευτῆς προσαγορεύσω ὄλβιον. Ἐλαχίστους γὰρ εὐδαίμονας εἶναι ἐν τῷ βίῷ νομίζω. Ἔτι δ’ ἐλαττους εὐτυχῆ θάνατον ἐτελεύτησαν. Οἱ γὰρ ἄνθρωποι καὶ ἐχθίων τῶν ἀνθρώπων ὁ τῶν θεῶν φθόνος τοῖς ἀρίστοις καὶ κρατίστοις αἰσχίστην καὶ κακίστη τὴν τοῦ βίου τελευτὴν ἐμηχανήσαντο.”

## 52. Z rodzinnych stron Sofoklesa
Πλησίον μὲν τῶν Ἀθηνῶν, πλησιαίτατα δὲ τῆς Ἀκαδεμείας ἦν ὁ Ἵππιος Κολωνός, ἡ τοῦ Σοφοκλέους πατρίς, ὑπὸ τοῦ ποιητοῦ καλῶς ἐγκωμιαζομένη. Ἐκεῖ γὰρ ἦν τὸ ἄλσος, ὃ δικαίως οἱ ποιηταὶ κάλλιστον ὀνομάζουσιν. Οὐδαμοῦ γὰρ τῆς Ἀττικῆς ἥδιον ὄζει νάρκισσος καὶ κρόκος καὶ ἴα. Ἐνθάδε καὶ ἀηδόνες ἥδιστα καὶ κάλλιστα ᾄδουσιν. Ἔστι δὲ καὶ ἱερὸς τόπος πάντως ἄβατος τοῖς θνητοῖς, οὗ διατρίβουσιν οἱ θεοί, μάλιστα δ’ αἱ Νύμφαι.

## 53. Moneta ateńska
Παρὰ τοῖς Ἀθηναίοις ἦν ἐν χρήσει ἀργυρᾶ νομίσματα, οὐδαμῶς δὲ χρυσᾶ, οὐδὲ χαλκᾶ. Τὸ τάλαντον οὐκ ἦν νόμισμα, ἀλλὰ βάρος ἀργυρίου· ἓν τάλαντον εἶχεν ἑξήκοντα μνᾶς· ἡ δὲ μνᾶ καὶ αὐτὴ βάρος ἐσήμαινεν· μία δὲ μνᾶ εἶχε δραχμὰς ἑκατόν. Ἡ δὲ δραχμὴ ἤδη νόμισμα ἦν. Εἶχον δ’ οἱ Ἀθηναῖοι χρῆσιν διδράχμων καὶ τετραδράχμων καὶ δεκαδράχμων· εἶχε δὲ τὸ δίδραχμον δύο δραχμάς. Ἡ δὲ δραχμὴ ὀβολοὺς εἶχεν ἕξ· πρὸς δ’ ἦν ἐν χρήσει διώβολα καὶ τριώβολα.

## 54. Królowie perscy
Κῦρος, ὁ πρῶτος τῶν Περσῶν βασιλεύς, ἐκράτησε μὲν τῶν Μήδων ἔτει τρίτῳ τῆς ἑβδόμης ὀλυμπιάδος καὶ πεντεκοστῆς, κατεστρέψατο δὲ Κροῖσον, τὸν Λυδῶν βασιλέα, καὶ ἐδούλωσεν Ἴωνας τοὺς ἐν τῇ Ἀσίᾳ. Δεύτερος δὲ τῶν Περσῶν βασιλεῦς Καμβύσης Αἴγυπτον κατεστρέψατο. Τρίτος ἦν Δαρεῖος, ὃς τοῖς Ἀθηναίοις σφοδρῶς ἐχαλέπαινεν, ὅτι τοῖς ἐν Ἀσίᾳ αὐτῶν ἀδελφοῖς οὖσιν Ἴωσιν ἐβοήθησαν. Τρὶς οἱ Πέρσαι ἐπὶ τοὺς Ἕλληνας ἐστρατεύσαντο, ὡς τὴν Ἑλλάδα καταστρεψόμενοι, δὶς μὲν Δαρείου βασιλεύοντος, τὸ τελευταῖον δὲ Ξέρξου, τοῦ τέταρτου τῶν Περσῶν βασιλέως.

## 55. Militiades i Kallimach
Πρὸ τῆς ἐν Μαραθῶνι μάχης Μιλιτιάδης ὁ Κίμωνος πρὸς Καλλίμαχον, τότε πολέμαρχον ὄντα, ἧκε καὶ ἔλεξεν· „Ἐν σοὶ νῦν ἐστιν, ὦ Καλλίμαχε, ἢ καταδουλῶσαι Άθήνας ἢ ἐλευθέρας ποιῆσαι. Ἐπεὶ δὲ τῶν στρατηγῶν, ὄντων δέκα, δίχα γίγνονται αἱ γνῶμαι, τῶν μὲν κελευόντων συμβάλλεσθαι, τῶν δ’ οὔ, ἱκετεύω σε τῇ ἐμῇ γνώμῃ προσχωρεῖσθαι καὶ τοῖς βαρβάροις αὐτικα διαμάχεσθαι. Πάντα νῦν ἐπὶ σοὶ ἐστιν. Εἰ γὰρ σὺ ἐμοὶ πείσῃ, ἔσται τάχα ἡ ἐμὴ καὶ σὴ πατρὶς ἐλευθέρα καὶ πρώτη τῶν ἐν τῇ Ἑλλάδι πόλεων.” Τοῦτο λέγων, ἔπεισε τὸν Καλλίμαχον.

## 56. Psy i wilki
Λύκοι κυσὶν ἔλεξαν· „Διὰ τί, τὰ πάντα ὅμοιοι ὄντες ἡμῖν, οὐ στέργετε ἡμᾶς ὡς ἀδελφοί, οὐδὲν γὰρ ὑμῶν διαλλάττομεν, πλὴν τῇ γνώμῃ; Καὶ ἡμεῖς μέν ἐσμεν ἐλεύθεροι, ὑμεῖς δέ, τῶν ἀνθρὠπων δοῦλοι ὄντες, πληγὰς παρ’ αὐτῶν λαμβάνετε καὶ φυλάττετε τὰ τῶν ὑμετέρων δεσποτῶν πρόβατα. Τί οὐ στέργομεν άλλήλους; Ἀλλ’ ἡμέτεροι γίγνεσθε ἑταῖροι, πάντα κοινὰ μεθ' ἡμῶν ἐσθίοντες.”

Ὑπήκουσαν οἱ κύνες, οἱ δὲ λύκοι εἰς τὸ σπήλαιν εἰσερχονται καὶ τοὺς κύνας διαφθείρουσιν.

## 57. Herkules na rozstajnych drogach (a)
Ἡρακεῖ ἐφ’ ἥβης ὄντι, ἡνίκα οἱ νέοι φαίνουσιν, εἴτε τὴν δι’ ἀρετῆς ὁδὸν τρέψονται ἐπὶ τὸν βίον εἴτε τὴν διά κακίας, συντυγχάνουσιν δύο γυναῖκές τινες. Ὧν ἡ μὲν ἑτέρα, εὐπρεπὴς καὶ ἐσθῆτα λευκὴν ἔχουσα, κατανοήσασα τὸν νεανίαν, προὔβαινε τὸν αὐτὸν τρόπον, ὥσπερ πρότερον, ἡ δ’ ἑτέρα, εὐτρεφὴς καὶ ἔχουσα ἐσθῆτα λαμπράν, ὡς τάχιστα προσέτρεχε καί· „Ὦ Ἠράκλεις”, ἔφη, „ἔμοιγε ἕπου. Εἰ γάρ τινος φίλη εἰμί, ἐκεῖνον τὴν ἡδίστην τε καὶ ῥᾴστην ὁδὸν ἄγω.”

## 58. Herkules na rozstajnych drogach (b)
Καὶ ὁ Ἡερακλῆς, ἀκούσας ταῦτα· „Τί”, ἔφη, „ὄνομα σοί ἐστιν;” Ἡ δ’· „Οἱ μὲν ἐμοὶ φίλοι”, ἔφη, „ὀνομάζουσί με Εὐδαιμονίαν, οἱ δ’ ἐχθροὶ Κακίαν.” Ἐν τοῦτῳ δ’ ἑτέρα ἔλεγε τάδε· „Καὶ ἐγὼ δ’ ἥκω πρὸς σέ, ὦ Ἡράκλεις, οὐκ ἄπειρος οὖσα, τίς εἶ καὶ ἐκ τίνων γονέων. Οὐκ ἐξαπατήσω δέ σε προοιμίοις ἡδονῆς, ἀλλὰ πάντα σοι διηγήσομαι μετ’ ἀληθείας· πάντων τῶν ἀγαθῶν οὐδὲν ἄνευ πόνου τοῖς ἀνθρώποις γίγνεται.” Καὶ ἡ Κακία· „Ἀλλ’, ὦ Ἡράκλεις, τί ἔτι μέλλεις, ταύτης ἀκουσας τοιοῦτον λόγον. Ἐγὼ ῥᾳδίαν καὶ βραχεῖαν ὁδὸν ἐπὶ τὴν εὐδαιμονίαν ἄξω σε.”

## 59. Herkules na rozstajnych drogach (c)
Καὶ ἡ Ἀρετή· „Ὦ τλῆμον, ποίαν δὲ σὺ εὐδαιμονίαν ἔχεις, ἥτις τοὺς σεαυτῆς διαφθείρεις φίλους; Πᾶς γάρ, ὅστις σοι ἕπεται, ἑαυτὸν βλάπτει καὶ τοσούτῳ μᾶλλον ὑπὸ τῶν θεῶν καὶ ἀνθρώπων ἀγαθῶν ἀτιμάζεται, ὅσῳ μᾶλλόν σοι πείθεται. Σὺ μὲν καὶ οἱ φίλοι σου πάντων ἡδίστου ἀκούσματος, ἐπαίνου ἀληθοῦς ὑμῶν ἀυτῶν, οὐδέποτ’ ἀκούετε. Ὅσοις δ’ ἐγὼ ἐντυγχάνω, οὗτοι πάντες εἰσὶν ἐν μεγάλῃ τιμῇ· ἔργον δὲ καλὸν ούδὲν χωρὶς ἐμοῦ γίγνεται.”

Ὁ δ’ Ἡρακλῆς. ταῦτ’ ἀκούσας, ἕπεται τῇ Ἀρετῇ.

## 60. Zeus, Prometeusz i Momus
Ζεύς ποτε Προμηθεῖ ἔλεξεν· „Ἐρίζωμεν, τίς κάλλιόν τι ποιήσει, Μῶμος δὲ κρινέτω τὰ ἡμέτερα ἔργα.” Καὶ ὁ μὲν Ζεὺς ταῦρον ἐποίησεν, ὁ δὲ Προμηθεὺς ἄνθρωπον. Μῶμος δέ, φθονήσας τῶν ἔργων, ἔφη· „Εἴθ’ ἔχοιμι τὴν ὑμετέραν δύναμιν. Ταῦτα γὰρ πάντα μεταποιῆσαι βούλομαι. Τῷ μὲν ταύρῳ τοὺς ὀφθαλμοὺς ἐπὶ τοῖς κέρασι προσάπτειν δεῖ, ἵνα βλέπῃ, ποῦ τύπτει, οἱ δ’ ἄνθρωποι θύρας ἐν στέρνοις ἐχόντων, ἵνα ἄλλοι τὰς γνώμας αὐτῶν γιγνώσκωσιν.” Καὶ ὁ Ζεύς ὀργιζόμενος τῷ Μῶμῳ, ἐκ τοῦ Ὀλύμπου ἀπεδίωξεν αὐτὸν εἰς ἀεί.

## 61. Mowa Kserksesa
Ξέρξης μετὰ τὴν Αἰγύπτου ἅλωσιν, ὡς ἔμελλεν ἐπὶ τὰς Ἀθήνας στρατεύσεσθαι, σύλλογον Περσῶν τῶν ἀρίστων ἐποιήσατο, ἵνα πυνθάνοιτο αὐτῶν, τίνα γνώμην περὶ τῆς στρατείας ἔχοιεν, καὶ τοιόνδε τινὰ λόγον ἐποιήσατο· „Ὦ Πέρσαι, συνέλεξα ὑμᾶς, ἵνα, ἃ βούλομαι πράττειν, ὑμῖν ἀποφαίνωμαι. Μέλλω δὲ στρατεύεσθαι διὰ τῆς Εὐρώπης ἐπὶ τὴν Ἑλλάδα, ἵνα οἱ Ἀθηναῖοι δῖκην ἐκτίνωσι τούτων, ἃ ἐποιήσαν τοὺς Πέρσας. Ὑμεῖς δ’ ἄν μοι βοηθήσαντες μάλιστα χαρίζοισθε.” Ταῦτα λέξας ἐπαύσατο.

## 62. Gnom
Ὅταν τι πράττῃς ἐσθλόν, καλὴν ἐλπίδα  
πρόβαλλε σαυτῷ, τοῦτο γιγνώσκων, ὅτι  
τόλμῇ δικαίᾳ θεὸς συλλαμβάνει.

## 63. Ostracyzm
Θεμιστοκλῆς διεβάλλετο πρὸς τὸν δῆμον, ὡς τυραννίδος ὀρέγοιτο. Ὁ δὲ δῆμος τοῖς ὄνομα καὶ δόξαν ὑπὲρ τοὺς πολλοὺς ἔχουσιν ἤχθετο. Ἧκον οὖν εἰς ἄστυ οἱ πολῖται, ἵνα ὀστρακίζοιεν τὸν Θεμιστοκλέα. Ἦν δ’ ὁ ὀστρακισμὸς τοιόνδε τι· ὅτε τινὰ ἀποδιῶξαι βούλοιντο, συνελέγοντο εἰς ἄστυ. Ὄστρακον ἐλάμβανεν ἕκαστος καὶ ἐγγράψας, ὅν ἐβούλετο ἀποδιῶξαι, ἔφερεν εἰς τόπον τινὰ τῆς ἀγορᾶς. Τῶν δ’ ἐν τέλει ἦν πρῶτον ἀριθμῆσαι τὸ πλῆθος τῶν ὀστράκων· εἰ γὰρ ἑξακισχιλίων ἐλάττους οἱ γράψαντες ἦσαν, ἄκυρος ἦν ὁ ὀστρακισμός· τὸν δ’ Ἀθηναῖον, ὃν ὑπὸ τῶν πλείστων ὠνομάζετο, ἐξεκήρυττον εἰς ἔτη δέκα. Οὕτως ἐξεκήρυξαν οἱ Ἀθηναῖοι τὸν Θεμιστοκλέα.

## 64. Ostatnie chwile panowania Kambizesa (a)
Ἐπειδὴ Καμβύσης, ὁ τῶν Περσῶν βασιλεύς, οὐκ εὐτύχει ἐπὶ τοὺς Αἰθίοπας στρατεύων, ἀπεχώρησεν εἰς Μέμφιν. Οἱ τὴν πόλιν ἐνοικοῦντες τότε εὐθύμουν χαίροντες, ὅτι ἔχοιεν τὸν Ἄπιν, ὃν πολὺν χρόνον ἐζήτουν. Ἀκούσας δ’ ὁ Καμβύζης τὰ περὶ τὸν Ἄπιν, τοῖς πολίταις ἔλεξεν· „Τοῖς μὲν ὑμετέροις λόγοις ἀπιστῶ, ὑμᾶς δ’ ὡς ψευδομένους ἀναιρεῖν κελεύω, ἵνα ἡ πόλις πενθῇ ἐμοῦ πενθοῦντος.” Παίσας δὲ τὸν Ἄπιν τῷ ξίφει, πρὸς τοὺς ἱερέας· „Ὦ κακαὶ κεφαλαί”, ἔφη, „νῦν τὸν ὑμέτερον θεὸν ὠφελεῖτε.”

## 65. Ostatnie chwile panowania Kambizesa (b)
Ἐνθυμούμενος δ' ὁ Καμβύσης, ὅτι οἱ Πέρσαι Σμέρδιν, ἀδελφὸν αὐτοῦ, περὶ πολλοῦ ποιοῖντο, Πραξάσπην, ὃν πιστότατον ἡγεῖτο, ἔπεμψεν ὡς φονεύσοντα τὸν ἀδελφόν. Μετὰ δὲ ταῦτα καὶ τὴν ἀδελφὴν αὐτὸς λέγεται φονεῦσαι, ὅτι ἐπὶ τῷ τοῦ ἀδελφοῦ θανάτῳ ἐφαίνετο λυπουμένη. Ταῦτα δὲ ποιήσας, τῷ Πραξάσπει ἔλεψεν· „Ποῖόν τινὰ με ἡγοῦνται οἱ Πέρσαι; Ἐνθυμοῦ, ὅτι σε τἀληθῆ λέγειν δεῖ.” Ὁ δ’· „Ὦ δέσποτα”, ἔφη, „τὰ μὲν ἄλλα πάντα μεγάλως ἐπαινεῖ, τῇ δὲ φιλοινίᾳ σε λέγουσι πλεόν κρατεῖσθαι.”

## 66. Ostatnie chwile panowania Kambizesa (c)
Ὁ δὲ Καμβύσης πρῶτον μὲν ἐσίγα, θαυμάζων τὸν Πραξάσπην, ὅτι τολμῴη τοιαῦτα πρὸς βασιλέα λέγειν, ἔπειτα δέ· „Νὴ Δία”, ἔφη, „ἔτι μᾶλλον ἀγαπῶ σε, ὦ Πράξασπες, ὅτι οὐκ ἐξαπατᾷς με ψευδέσι λόγοις. Νῦν δ' ὅρα, εἰ οἱ Πέρσαι τἀληθῆ λέγουσιν. Ἐὰν μὲν οὖν τὸν παῖδά σου, ὃν καθεζόμενον ὁρᾷς, διὰ μέσης τῆς καρδίας τοξεύω, οὐδεὶς ἔτι τολμήσει λέγειν ἐμὲ μεθύειν. Ἐὰν δὲ μή, ὑμῶν ἡ γνώμη νικήσει.” Τοξεύσαντος δὲ τοῦ Καμβύσου καταπίπτει ὁ παῖς καὶ τελευτᾷ.

## 67. Ostatnie chwile panowania Kambizesa (d)
Θεώμενος τὴν τοῦ βασιλέως μανίαν ὁ Κροῖσος, ἐπειρᾶτο ἐκεῖνον νουθετεῖν· ἔλεγεν οὖν τῷ Καμβύσῃ τάδε· „Ὦ βασιλεῦ, ταῦτα ποιῶν, οὐκ ἂν κτῷο φίλους, ἀλλ’ ἀποβάλλοις ἄν, οὓς ἔχεις. Πειρῶ τῶν ἐπιθυμιῶν κρατεῖν, ὅπως μὴ πάντες σε αἰτιῶνται.” Ὁ δὲ Καμβύσης τοὺς θεράποντας ἐκέλευσε φονεῦσαι αὐτόν. Οἱ δὲ κατέκρυψαν ἐκεῖνον. Ἐπειδὴ δ’ ὕστερον οὐ πολλῷ βασιλεὺς ἐπόθησε τὸν Κροῖσον ὁρᾶν, ἀκούσας αὐτὀν περιόντα τὸ μὲν ἥδετο, τὸ δ’ ἠνιᾶτο ἐπὶ τῇ ἀπιστίᾳ τῶν θεραπόντων.

## 68. Ostatnie chwile panowania Kambizesa (e)
Ἐν δὲ τοῖς Πέρσαις στάσιν πρὸς Καμβύσην ἐποιήσαντο Μάγοι δύο, ἀδελφοί, οἳ ἐπειρῶντο τὴν τῶν Μήδων ἀρχὴν ἐπανορθοῦν. Ὁ μὲν τῶν Μάγων, ὄνομα Σμέρδις, προσεποιεῖτο ἀδελφὸς εἶναι τοῦ Καμβύσου· οἱ δὲ Πέρσαι, πάλαι ἀγανακτοῦντες, ὅτι βασιλεὺς ἄνδρας τοὺς ἀναιτίους θανάτῳ ζημιοίη, ἠξίουν Σμέρδιν βασιλέα αὐτῶν εἶναι. Ὁ δὲ Καμβύσης, ταῦτα ἀκούσας, δῆλος ἦν ἀνιώμενος, ὅτι ὁ Πραξάσπης αὐτὸν ἐψεύσατο, οὐδὲ τὸν ἀδελφὸν ἐφόνευσεν.

## 69. Ostatnie chwile panowania Kambizesa (f)
Μεταπεμψάμενος δὲ τὸν Πραξάσπην· „Ὦ Πράξασπες”, φησίν, „ὑπὸ κέρυκος δελοῦται, ὅτι ὁ ἐμὸς ἀδελφὸς οὐκ ἐτελεύτησεν. Πῶς οὖν τολμᾷς τοῖς ἐμοῖς λόγοις ἐναντιοῦσθαι;” Ὁ δὲ Πραξάσπης· „Ὦ δέσποτα”, ἔφη, „θυμοῖ νῦν ἐμοί, ἄλλοις πειθόμενος. Τάχα δ' ἂν δηλοῖτο, ὅτι ἀεὶ τῆς σῆς πίστεως ἄξιος ἦν. Ὁ δὲ τοὺς Πέρσας δουλοῦσθαι βουλόμενος καλεῖται Σμέρδις. Ἔστι δὲ Μάγος τις.”

Βασιεύς, ἀκούσας ταῦτα, ἐπὶ τοὺς Μάγους στρατεύεται, ἀλλ’ ἐπὶ τὸν ἵππον ἀναβαίνων, τιτρώσκεται τῷ ξίφει καὶ τελευτᾷ.

## 70. Gnomy
1. Ὅτ’ εύτυχεῖς μάλιστα, μὴ φρόνει μέγα.
2. Μιμοῦ τὰ σεμνά, μὴ κακοὺς μιμοῦ τρόπους.
3. Γονεῖς σὺ τίμα καὶ φίλους εὐεργέτει.

## 71. Igrzyska olimpijskie
Οἱ Ἕλληνες ἐν πολλῇ τιμῇ ἐποιοῦντο τοὺς ἀθλητάς, μάλιστα δ’ ἐτιμῶντο οἱ ἐν τοῖς Ὀλυμπιακοῖς ἀγῶσι νικήσαντες. Ἐν δὲ τοῖς ἀγῶσιν, διὰ τεττάρων ἐτῶν ἐν Ὀλυμπίᾳ γιγνομένοις, ἠγωνίζοντο οἱ Ἕλληνες δρόμῳ καὶ πάλῃ καὶ πυγμῇ καὶ ἅρμασι καὶ ἵπποις καὶ τῷ πεντάθλῳ. Οἱ δὲ νίκήσαντες ἆθλον μὲν οὐδὲν πλὴν στέφανον ἐλάας, πολλὰς δὲ τιμὰς ἐδέχοντο. Τῶν νικηφόρων εἰς τὴν ἑαυτῶν πόλιν ἀπερχομένων οἱ πολῖται μέρος κατέβαλλον τῶν τειχῶν καὶ διὰ τούτου τοὺς νικήσαντας εἰσῆγον. Τοῦτο δὲ ποιοῦντες, ἐδήλουν, ὅτι οὐ δεῖται τειχῶν πόλις, ἥτις τοιούτους ἔχει πολίτας.

## 72. Myśliwy i kuropatwa
Πέρδικά τις θηρεύσας, ἔμελλε φονεῦσαι. Ἡ δ’ ἱκέτευεν, λέγουσα· „Ἔασόν με ζῆν· ἐάν με ἀπολύσῃς, σοὶ δουλεύσω. Ἐπαγγέλλομαι άντ’ ἐμοῦ πολλάς σοι πέρδικας κυνηγετήσειν. Τοιούτῳ δὲ τρόπῳ πολλὰς ἂν πέρδικας θηρεύσαις καὶ τοξεύσαις. Ἦ μὴν τὸν σὸν ἔπαινον κομιῶ. Πίστευσόν μοι μηδ’ ἀνανεύσῃς.”

Ὁ δὲ κυνηγέτης· „Δι’ αὐτὸ τοῦτο”, ἔφη, „μᾶλλόν σε φονεύσω. Λέγεις γάρ, ὅτι τοὺς φίλους σου ἐνεδρεύσεις.”

Ὁ μῦθος δηλοῖ, ὅτι ὁ τοῖς φίλοις ἐπιβουλεύων αὐτὸς ταῖς ἑαυτοῦ ἐνέδραις ἐμπίπτει.

## 73. Lew i mysz
Λέοντος κοιμωμένου μῦς τῲ στόματι ἐμίπτει· ὁ δὲ θεασάμενος, ἐβούλετο τὸν μῦν φονεῦσαι. Ὁ δὲ μῦς ἱκέτευε· „Φεῖσαί μου, κράτιστε βασιλεῦ.” Ἐπειδὴ δ’ ὁ λέων αὐτὸν ὰπέλυσεν, ὁ μῦς· „Εἴθε ποτέ σοι”, ἔφη, "μικὸς ὤν, ἀντιχαρισαίμην.” Οὐ πολὺ ὕστερον ἐμπίπτει ὁ λέων τῷ δυκτύῳ. Τότε δ’ ὁ μῦς, ἀκούσας αὐτοῦ στένοντος, προστρέχει καὶ τὸ δίκτυον περιτρώγει. Τὸν δὲ λέοντα λύσας· „Νῦν οὖν ὁρᾷς", ἔφη, „ὄτι ἔστι καὶ παρὰ τοῖς μικροῖς μυσὶ χάρις.”

## 74. Smerdys Samozwaniec (a)
Τελευτήσαντος Καμβύσου Σμέρδις, ὁ Μάγος, τῶν Περσῶν ἐβασίλευεν, πάντας εὐεργετῶν, ἤλπιζε γὰρ οὕτω μάλιστα τοὺς Πέρσας τῇ πίστει ἐμμενεῖν. Ὀτάνης, ἀνὴρ ἐν τοῖς Πέρσαις ἐνδοξότατος, ὑποπτεύων υἱὸν Κύρου τὸν Σμέρδιν οὐκ εἶναι, Φαιδύμην, τὴν ἑαυτοῦ παῖδα, ἣ τοῦ Σμέρδεος ἦν γυνή, πρσέφη τοῖσδε· „Εἰ ἀποκτενοῦμεν τὸν Μάγον, σύ τε μὲν οὐκ αἰσχυνῇ, τῷ ἀγεννεῖ συνοικοῦσα, καὶ οἱ Πέρσαι δ’ εὐφρανοῦνται. Σὺ δὲ πάντα κρινεῖς. Εὕδοντος αὐτοῦ τῶν ὤτων ἅψαι· εἰ φανεῖται μὴ ἔχων τὰ ὦτα, Μάγῳ νόμιζε, οὐ βασιλεῖ συνοικεῖν. Ὁ γὰρ Μάγος ὦτα οὐκ ἔχει Κύρου ποτ' αὐτὸν οὕτως τραυματίσαντος.”

## 75. Smerdys Samozwaniec (b)
Ἡ δ’ οὖν Φαιδύμη, ῥᾳδίως κρίνασα οὐκ ἔχοντα τὸν Σμέρδιν τὰ ὦτα, πρὸς τὸν Ὀτάνην ἀπέστειλε πιστόν τινα ἀγγελοῦντα τὸ πρᾶγμα. Ὁ δ’ Ὀτάνης, συναγείρας πρώτους τῶν Περσῶν, ἐσήμηνε τοῦτο. Ἐδόκει οὖν τοῖς Πέρσαις ἀποκτεῖναι τὸν Μάγον, περὶ τοῦδε δὲ διεφέροντο, πότερον αὐτίκα δέοι περᾶναι τὸ ἔργον ἢ πρότερον πλείους προσκτήσασθαι ἑταίρους. Τέλος δὲ Δαρεῖος ὁ Ὑστάσπου γνώμην ἀπεφήνατο, ὅτι χρὴ τῶν Μάγον τυραννίδα ἀμύνασθαι καὶ τὸν Σμέρδιν αὐτίκα ἀποκτεῖναι. Καὶ οὐ περιέμειναν οἱ Πέρσαι, ἀλλ’ ἐπέραναν αὐτίκα τὸ ἔργον.

## 76. Zwyczaje Lacedemońskie (a)
Οἱ Λακεδαιμόνιοι σιδηροῖς νομίσμασιν ἐχρῶντο. Δέκα οὖν μνᾶς σιδήρου ἐν μεγάλῃ ἀποθήκῃ ἔδει διαφυλάττεσθαι. Οὕτως δὲ πολλὰ γὲνη ἀδικημάτων ἀπεκωλύθη τῆς Λακεδαίμονος· τίς γὰρ ἂν ἢ κλέπτειν πεισθείη ἢ ἁρπάζειν, ὃ κατακρύψαι ἀδύνατον ἦν. Καὶ τάδε τῷ Λακεδαιμονίων νόμῳ ταχθῆναι λέγεται· οὐ θεμιτὸν ἦν τοῖς ἐλευθέροις χρεματίσασθαι, ἵνα μὴ δουλωθεῖεν τῷ χρυσῷ. Εἴ τί που χρυσοῦ ἢ ἀργύρου ἐκρύφθη, οἱ ἔχοντες ἰσχυρῶς ἐζημιώθησαν.

## 77. Zwyczaje Lacedemońskie (a)
Γυμνάσια δ’ ὥσπερ νεανιῶν ἦν, οὕτω καὶ παρθένων. Διά τοῦτο αἱ Λάκαιναι, τῶν ἄλλων κρείττονες οὖσαι, ἐπὶ τῇ τῶν τρόπων βεβαιότητί τε καὶ τῇ ἀνδρείᾳ ἀεὶ θαυμασθήσονται. Ἀεὶ ὑμνηθήσεται ἐκείνη ἡ Λάκαινα γυνή, ἣ τοῦ υἱοῦ αὐτῆς ἐν μάχη χωλευθέντος καὶ δυσφοροῦντος ἐπὶ τούτῳ ἔφη· „Εὐφράνθητι, τέκνον, μηδ’ αἰσχυνθῇς τὴν πήρωσιν, καθ’ ἕκαστον γάρ σου τὸ βῆμα τῆς ἰδίας ἀρετῆς μνημονεύσεις.”

## 78. Prawda
Οὐκ ἔστιν οὔτε ζωγράφος, μὰ τοὺς θεούς,  
οὔτ’ ἀνδριαντοποιός, ὅστις ἂν πλάσαι  
κάλλος τοιοῦτον, οἷον ἀλήθει’ ἔχει.

## 79. Cześć bohaterom (a)
Ἐπὶ ταῖς κλίναις τῶν στρατιωτῶν τῶν ἐν τῇ μάχῃ τελευτησάντων τοιόνδε τινά ἔπαινον ἔλεγεν ὁ στρατηγός· „Ἡ ἡμετέρα πατρὶς ἐσχάτως ἐκεκινδυνεύκει, ὁ γὰρ πολέμιος στρατευσάμενος τὴν ἡμετέραν χώραν κακὰ ἔδρα. Τί οὗτοι ἐποίησαν; Ἀνδρείως μαχόμενοι ἠλευθερώκασι μὲν τὴν πόλιν, νικῶντες δ’ εὐκλεῶς τετελευτήκασιν. Νενίκηκεν οὖν ἡ ἡμετέρα πόλις καὶ κεκράτηκεν τιμωρησαμένη τοὺς πολεμίους. Ἀλλ’ οὐ ῥᾳδία ἦν ἡ νίκη· ὅμως οἱ ἀνρεῖοι στρατιῶται ἐνίκησαν τοὺς πολεμίους.

## 80. Cześć bohaterom (b)
Παρηγγέλκαμεν δ’ ὑμῖν, ὧ πολῖται, τούτους τὴν τῶν πολεμίων δύναμιν διεφθαρκέναι καὶ μαχομένους τετελευτηκέναι. Αὐτῶν δ’ ὁ θάνατος πολλῶν μὲν έλπίδας πέπαυκεν, πολλοῖς δὲ λύπας καὶ ἀλγηδόνας ἐμπεποίηκεν, πάντων δ’ εὐθυμίαν λέλυκεν. Πάντες οὖν ὀδύρονται τοὺς τὴν μεγίστην ἀνδρείαν πεφαγκότας, οἷς εἰς ἀεὶ δόξα ἀθάνατος ἔσται καὶ χάρις παρὰ πάντων τῶν Ἑλλήνων. Νῦν δέ, τὰ νόμιμα θύσαντες, οἴκαδε ἀπονοστεῖτε.”

## 81. Harmodios i Arystogejton (a)
Ἐπεὶ Ἱππίας καὶ Ἵππαρχος, οἱ Πεισιστράτου παῖδες, χαλεπῶς ἐτυράννευον, Ἀριστογείτων, Ἀθηναῖός τις, ἔφη τάδε πρὸς Ἁρμόδιον, ὅς ἠδίκητο ὑφ’ Ἱππάρχου· „Ὦ Ἀρμόδιε, πρότερον μὲν ἐτετίμησο, νῦν δ’ τυράννου ὕβρισαι καὶ βέβλαψαι, ὥσπερ πολλοὶ πολῖται ὕβρισμένοι εἰσὶ καὶ βεβλαμμένοι. Εἰς δὲ τὸ δουλεύειν οὐ πεπαιδεύμεθα. Προτρέψωμεν οὖν ἄλλους, ὑπὸ τῶν τυράννων δεδιωγμένους, καὶ ἐλευθερώσωμεν, ὥσπερ τοῖς νόμοις τέτακται, τὴν πατρίδα, ἥπερ πλεῖστον χρόνον ἐν τῃ ἐλευθερίᾳ ἐτέθραπτο.”

## 82. Harmodios i Arystogejton (b)
Ἄλλους δὲ φίλους παρώρμησεν ὁ Ἀριστογείτον, λέγων τάδε· „Υπὸ μὲν Θησέως, ὅς μέγιστος τῆς πόλεως εὐεργέτης ἀναγέγραπται, ἡ πατρὶς πολλῶν κακῶν ἀπαλλαχθῆναι λέγεται. Ἥ τε πόλις ᾤκισται ὑπ’ αὐτοῦ καὶ ἡ βασιλεία ἵδρυτο. Κόδρου δὲ τελευτήσαντος οὐδεὶς ἐτυράννευσε πλὴν τοῦ Πεισιστράτου καὶ τῶν Πεισιστρατιδῶν. Νῦν δὴ ἡ ἐλευθερία διέφθαρται. Πότε λελύσεται ἡ πόλις τῶν τυράννων; Ἀνάγκη τοίνυν ἐστὶν αὐτίκα τοὺς τυράννους πεφονεῦσθαι.” Καὶ ἠλευθέρωσαν οἱ Ἀθηναῖοι τὴν πόλιν, τὸν μὲν Ἵππαρχον ἀποκτείναντες, τὸν δ’ Ἱππίαν ἀποδιώξαντες.

## 83. Per aspera ad astra
Πόνος γε, ὡς λέγουσιν, εὐκλείας πατήρ.

Τῶν γὰρ ἀγαθῶν καὶ καλῶν οὐδὲν ἄνευ πόνου καὶ ἐπιμελείας οἱ θεοὶ άπονέμουσιν τοῖς ἀνθρώποις. Ἀλλ’ εἴτε τοὺς θεοὺς εὐμενεῖς εἶναι βούλει, θεραπευτέον τοὺς θεούς· εἴτε ὑπὸ φίλων ἐθέλεις ἀγαπᾶσθαι, τοὺς φίλους εὐεργετηέον· εἴτε ὑπό τινος πόλεως ἐπιθυμεῖς τιμᾶσθαι, ἡ πόλις σοί ἐστιν ὠφελητέα· εἰ δὲ καὶ τῷ σώματι βούλει δυνατὸς εἶναι, τὸ σῶμα γυμναστέον σὺν πόνοις καὶ ἱδρῶτι. Ἡ μὲν ἀργία σε, ὦ νεανίσκε, μισητὸν ποιήσει, ὁ δὲ πόνος κλυτὸν καὶ ζηλωτόν.

## 84. Kadmus (a)
Ἐπεὶ Ζεὺς Εὐρώπην τὴν τοῦ Ἀγήνορος, τοῦ Φοινίκων βασιλέως, ἀπήγαγεν, ὁ πατὴρ Κάδμον, τὸν υἱόν, ἐπὶ ζήτησιν τῆς παρθένου ἐκπέμπων, ἔλεξεν· „Τὴν πατρίδα σε καταλιπεῖν κελεύω, τὴν ἀδελφὴν ζητήσοντα. Ὁ τὴν Εὐρώπην ἡρπακὼς διὰ τῆς θαλάττης εἰς νήσους τινὰς κατέφυγεν. Καὶ κομίζων μὲν αὐτήν, ἄνελθε εἰς τὴν πατρίδα· εἰ δὲ μή, αὐτὸς μὴ ἀνέλθῃς.” Ὁ οὖν Κάδμος, τῷ πατρὶ πειθόμενος, εἰς Δελφοὺς ἦλθε καὶ ἐμαντεύσατο παρὰ τῷ Ἀπόλλωνι, ὅπος ἂν τὴν ἁρπασθεῖσαν ἀδελφὴν τῷ πατρὶ ἀναγάγοι.

# 85. Kadmus (b)
Ὁ δὲ θεὸς ἔχρησεν· „Ἐὰν ἐν πεδίῳ τινὶ περιερχόμενος ἴδῃς ταῦρον, ἐργασίας ἐλεύθερον ὄντα, τὴν ὁδὸν ἐκείνου τραποῦ. Ἐν ᾧ δ’ ἂν τόπῳ ἐκεῖνος ἀναπαύσηται, πόλιν κτίσον.” Ἀπελθὼν δὴ κατὰ τοῦ Παρνασσοῦ, ὁ Κάδμος ταῦρον εἶδεν, ᾧ κατόπισθεν σπόμενος ἦλθεν εἰς τὴν Βοιωτίαν. Ἐπεὶ δ’ ἐν πεδίῳ τινὶ ὁ ταῦρος κατεκλίθη, ὁ Κάδμος τοῖς ἑταίροις εἶπεν· „Ὦ φίλοι, ἑσπόμην τῷ ταύρῳ, ὥς με ὁ θεὸς ἐκέλευσεν. Νῦν δέ, ἐπεὶ ἥκομεν, οὗ ὁ θεὸς ἡμᾶς ἐκέλευσε πόλιν κτίσαι, τραπώμεθα πρὸς τὸ ἔργον.” Οὕτως ὁ Κάδμος πόλιν κτίσαι λέγεται, Θήβας ὄνομα.
